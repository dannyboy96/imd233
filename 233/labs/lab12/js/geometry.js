var g1 = prompt("Enter a radius value:");
var g2 = prompt("Enter a radius value:");
var g3 = prompt("Enter a radius value:");

//store user radius input
var arr = [g1, g2, g3];

function calcCircleGeometries(radius) {
    if (isNaN(radius) || radius <= 0) {
        alert("Must enter a number that is greater than zero!!!");
        location.reload();
    }
    const pi = Math.PI;
    var area = pi * radius * radius;
    var circumference = 2 * pi * radius;
    var diameter = 2 * radius;
    var geo = [area, circumference, diameter];    
    return geo;
}

for (i = 0; i < arr.length; i++) {
    var geometries = calcCircleGeometries(arr[i]);
    var temp = document.getElementById("radius" + (i + 1));
    temp.textContent = arr[i];

    temp = document.getElementById("area" + (i + 1));
    temp.textContent = geometries[0];

    temp = document.getElementById("circumference" + (i + 1));
    temp.textContent = geometries[1];
    
    temp = document.getElementById("diameter" + (i + 1));
    temp.textContent = geometries[2];
}

//var geometries = [];
// for (var i = 0; i < arr.length; i++) {
//     var geometries = (calcCircleGeometries(arr[i]));

//     var temp = document.getElementById("a" + (i + 1));
//     temp.innerHTML = geometries[0]; 

//     temp = document.getElementById("c" + (i + 1));
//     temp.innerHTML = geometries[1]; 

//     temp = document.getElementById("d" + (i + 1));
//     temp.innerHTML = geometries[2]; 
// }

//store geo values from radius that user inputed
// var dict = [];
// for (var i = 0; i < arr.length; i++) {
//     dict.push({
//         key: arr[i],
//         value: geometries[i]
//     });
// }

// var table = document.getElementsByTagName("tbody").item(0);
// for (var i = 0; i < arr.length; i++) {
//     var row = document.createElement("tr");
//     for (var j = 0; j < arr.length; j++) {
//         var cell = document.createElement("td");
//         var textNode = document.createTextNode(geometries[i]);
//         cell.appendChild(textNode);
//         row.appendChild(cell);
//     }
//     table.appendChild(row);
// }

// document.getElementById("a1").innerHTML = calcCircleGeometries(g1);
// document.getElementById("c1").innerHTML = calcCircleGeometries(g1);
// document.getElementById("d1").innerHTML = calcCircleGeometries(g1);