var arr = ["Volkswagen", "Jetta", 2004, 20000];
var arr1 = ["Subaru", "WRX STI", 2020, 40000];
var arr2 = ["Dacia", "Logan", 2009, 8000];
var arr3 = ["Volkswagen", "R32", 2019, 38000];
var arr4 = ["Porsche", "911", 1975, 55000];

var arr2d = [arr, arr1, arr2, arr3, arr4];

//more elegant way
var table = document.getElementsByTagName("tbody").item(0);
for(var i = 0; i < arr2d.length; i++) {
    var row = document.createElement("tr");
    for(var j = 0; j < arr2d[0].length; j++) {
        var cell = document.createElement("td");
        var textNode = document.createTextNode(arr2d[i][j]);
        cell.appendChild(textNode);
        row.appendChild(cell);
    }
    table.appendChild(row);
}

//brute force method
document.getElementById("m1").innerHTML = arr2d[0][0];
document.getElementById("mo1").innerHTML = arr2d[0][1];
document.getElementById("y1").innerHTML = arr2d[0][2];
document.getElementById("p1").innerHTML = arr2d[0][3];

document.getElementById("m2").innerHTML = arr2d[1][0];
document.getElementById("mo2").innerHTML = arr2d[1][1];
document.getElementById("y2").innerHTML = arr2d[1][2];
document.getElementById("p2").innerHTML = arr2d[1][3];

document.getElementById("m3").innerHTML = arr2d[2][0];
document.getElementById("mo3").innerHTML = arr2d[2][1];
document.getElementById("y3").innerHTML = arr2d[2][2];
document.getElementById("p3").innerHTML = arr2d[2][3];

document.getElementById("m4").innerHTML = arr2d[3][0];
document.getElementById("mo4").innerHTML = arr2d[3][1];
document.getElementById("y4").innerHTML = arr2d[3][2];
document.getElementById("p4").innerHTML = arr2d[3][3];

document.getElementById("m5").innerHTML = arr2d[4][0];
document.getElementById("mo5").innerHTML = arr2d[4][1];
document.getElementById("y5").innerHTML = arr2d[4][2];
document.getElementById("p5").innerHTML = arr2d[4][3];


