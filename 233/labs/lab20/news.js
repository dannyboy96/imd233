//Your API key is: c95ff2e9cb53435e8f413ef505111d78

// $(document).ready(function() {
//     function Search() {
//         this.url = 'https://newsapi.org/v2/everything?' +
//         'q=apple&from=2018-06-02&to=2018-06-02'+ '&' +
//         'sortBy=popularity&' +
//         'apiKey=c95ff2e9cb53435e8f413ef505111d78';
//     };
    
//     $(".search").ready(function() {
//         var searchRequest = new Search();  
//         var request = new Request(searchRequest.url);        
//         var results = null;
        
//         fetch(request)
//             .then(function(response) {
//                 return response.json();
//             })
//             .then(function(results) {
//                 var element = document.getElementById("blogs");
//                 element.innerHTML = "";
//                 for (var i = 0; i < results.articles.length; i++) {
//                     element.innerHTML += "<div class=\"blog-post\">" + 
//                                         "<img class=\"img-responsive\" src=\"" + results.articles[i]['urlToImage'] + "\" height=\"400\">" +
//                                         "<a href=\"" + results.articles[i]['url'] + "\">" + 
//                                         "<h2 class=\"blog-post-title text-dark\">" + results.articles[i]['title'] + "</h2>" +
//                                         "<h5 class=\"blog-post-info text-muted\">" + results.articles[i]['description'] + "</h5>" + 
//                                         "</a>" +
//                                         "<p class=\"blog-post-meta\"> Source: " +
//                                         "<a href=\"" + results.articles[i]['source']['name'] + "\">" + results.articles[i]['source']['name'] + "</a>" +
//                                         " published at " + results.articles[i]['publishedAt'] + "</p>" +
//                                         "</div>";
//                 }
//             })
//     }) 

    
// });

$(function() {
    var url = 'https://newsapi.org/v2/everything?' +
             'q=apple&from=2018-06-02&to=2018-06-02'+ '&' +
             'sortBy=popularity&' +
             'apiKey=c95ff2e9cb53435e8f413ef505111d78';

    var request = new Request(url);
    fetch(request)
        .then(function(response) {
        //response now contains the json
        return response.json();
        })
        .then(function(results){
            var element = document.getElementById("blogs");
                 element.innerHTML = "";
                 for (var i = 0; i < results.articles.length; i++) {
                     element.innerHTML += "<div class=\"blog-post\">" + 
                                         "<img class=\"img-responsive\" src=\"" + results.articles[i]['urlToImage'] + "\" height=\"400\">" +
                                         "<a href=\"" + results.articles[i]['url'] + "\">" + 
                                        "<h2 class=\"blog-post-title text-dark\">" + results.articles[i]['title'] + "</h2>" +
                                         "<h5 class=\"blog-post-info text-muted\">" + results.articles[i]['description'] + "</h5>" + 
                                         "</a>" +
                                         "<p class=\"blog-post-meta\"> Source: " +
                                         "<a href=\"" + results.articles[i]['source']['name'] + "\">" + results.articles[i]['source']['name'] + "</a>" +
                                         " published at " + results.articles[i]['publishedAt'] + "</p>" +
                                         "</div>";
                 }
        })
});

