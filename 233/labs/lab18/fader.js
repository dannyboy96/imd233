$(document).ready(function() {
    $('li').css('margin', '10px');
    $('li').attr('id', 'uw');

    //Fadeout
    $('#p1 li').click(function() {
        console.log("$(this):" + $(this));
        $(this).fadeOut(2000, function() {
            console.log("fadeout complete!")
        });
    });

    //FadeIn
    $('#p2 li').click(function() {
        console.log("$(this):" + $(this));
        $(this).fadeOut(2000, function() {
            console.log("fadeout complete!")
        });
        $(this).fadeIn(2000, function() {
            console.log("fadein complete!")
        });
    });

    //FadeTo
    $('#p3 li').click(function() {
        $(this).fadeTo(2000, 0.4);
    });
    //FadeToggle
    $('#p4 li').click(function() {
        $(this).fadeToggle();


        var isHidden = $("#p4 >li").is(":hidden");
        if (isHidden) {
            console.log("All buttons hidden in fadeToggle()");
        }

    });
});