var stockData = [
    {
        name: "Microsoft",
        marketCap: "$381.7B",
        sales: "$86.8B",
        profit: "$22.1B",
        numOfEmployee: "128,000"
    },
    {
        name: "Symetra Financial",
        marketCap: "$2.7B",
        sales: "$2.2B",
        profit: "$254.4B",
        numOfEmployee: "1,400"
    },
    {
        name: "Micron Technology",
        marketCap: "$37.6B",
        sales: "$16.4B",
        profit: "$3.0B",
        numOfEmployee: "30,400"
    },
    {
        name: "F5 Networks",
        marketCap: "$9.5B",
        sales: "$1.7B",
        profit: "$311.2B",
        numOfEmployee: "3,834"
    },
    {
        name: "Expedia",
        marketCap: "$10.8B",
        sales: "$5.8B",
        profit: "$398.1M",
        numOfEmployee: "18,210"
    },
    {
        name: "Nautilus",
        marketCap: "$476M",
        sales: "$274.4M",
        profit: "$18.8M",
        numOfEmployee: "340"
    },
    {
        name: "Heritage Financial",
        marketCap: "$531M",
        sales: "$137.6M",
        profit: "$21M",
        numOfEmployee: "748"
    },
    {
        name: "Cascade Microtech",
        marketCap: "$239M",
        sales: "$136M",
        profit: "$9.9M",
        numOfEmployee: "449"
    },
    {
        name: "Nike",
        marketCap: "$83.1B",
        sales: "$27.8B",
        profit: "$2.7B",
        numOfEmployee: "56,500"
    },
    {
        name: "Alaska Air Group",
        marketCap: "$7.9B",
        sales: "$5.4B",
        profit: "$605M",
        numOfEmployee: "14,952"
    }
];

function genTable() {
    var table = document.getElementsByTagName("tbody").item(0);

    for (var i = -1; i < stockData.length; i++) {
        if (i == -1) {
            table.innerHTML = "<th>Company</th> <th>Market Cap</th> <th>Sales</th> <th>Profits</th> <th>Employees</th>"
        } else {
            table.innerHTML += "<tr><td>" + stockData[i]["name"] + "</td><td>" + stockData[i]["marketCap"] + "</td><td>" + stockData[i]["sales"] + "</td><td>" + stockData[i]["profit"] + "</td><td>" + stockData[i]["numOfEmployee"] + "</td></tr>";
        }
    }
}