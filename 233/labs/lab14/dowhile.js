

 var state = "IDLE";
do {
   
    var cmd = prompt("Enter a command:" + state, "next");
    switch (state) {
        case "IDLE":
            {
                if (cmd === "run" || cmd === "next") {
                    state = "S1";
                }
            }
            break;
        case "S1":
            {
                if (cmd === "next") {
                    state = "S2";
                } else if (cmd === "skip") {
                    state = "S3";
                } else if (cmd === "prev") {
                    state = "S4";
                }
            }
            break;
        //continue on with the rest
        case "S2":
            {
                if (cmd === "next") {
                    state = "S3";
                }
            }
            break;
        case "S3":
            {
                if (cmd === "next") {
                    state = "S4";
                } else if (cmd === "home") {
                    state = "IDLE";
                }
            }
            break;
        case "S4":
            {
                if (cmd === "next") {
                    state = "S1";
                }
            }
            break;
    }

} while (cmd != "exit" && cmd != "quit");