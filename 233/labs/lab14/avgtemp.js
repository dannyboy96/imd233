var wx_data = [
  {
    day: "Fri",
    hi: 82,
    lo: 55
  },
  {
    day: "Sat",
    hi: 75,
    lo: 52
  },
  {
    day: "Sun",
    hi: 69,
    lo: 52
  },
  {
    day: "Mon",
    hi: 69,
    lo: 48
  },
  {
    day: "Tue",
    hi: 68,
    lo: 51
  }
];

var avg = wx_data.reduce(function(val,d,i,ar)
{
    val.hi += d.hi;
    val.lo += d.lo;
    if(i == ar.length - 1)
    {
        val.hi /= ar.length;
        val.lo /= ar.length;
        return val;
    }
    else{
        return val;
    }
})

var table = document.getElementsByTagName("tbody").item(0);

for(var i = -1; i < wx_data.length; i++) {
  if(i == -1) {
    table.innerHTML = "<th>Day</th> <th>Low</th> <th>High</th>"
  } else {
    table.innerHTML += "<tr><td>" + wx_data[i]["day"] + "</td><td>" + wx_data[i]["hi"] + "</td><td>" + wx_data[i]["lo"] +"</td></tr>";
  }
}

var data = document.getElementById("output");
data.innerHTML = "<p><b>Average high:</b> " + avg.hi + "</p><p><b>Average low:</b> " + avg.lo + "</p><p><b>Overall average:</b> " + (parseFloat(avg.hi) + parseFloat(avg.lo)) / 2 + "</p>";


//var avg = wx_data.group().reduce(
//   function reduceHiAvg(p,v){
//     p.sum += v.hi;
//     console.log(p.sum);
//     p.hiAvg = p.sum / wx_data.length;
//     console.log(p.hiAvg);
//     return p;
//   },
//   function reduceLoAvg(p,v){
//     p.sum += v.lo;
//     console.log(p.sum);
//     p.loAvg = p.sum / wx_data.length;
//     console.log(p.loAvg);
//     return p;
//   }
// )

// function getAvg(wx_data) {
//   return wx_data.reduce(function (p, c) {
//     return p + c;
//   }) / wx_data.length;
// }

// 
console.log(avg);