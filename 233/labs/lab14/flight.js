function Flight(airline, number, origin, dest, depTime, arrivalTime, arrivalGate) {
    this.airline = airline;
    this.number = number;
    this.origin = origin;
    this.dest = dest;
    this.depTime = depTime;
    this.arrivalTime = arrivalTime;
    this.arrivalGate = arrivalGate;
    this.deltaTime = gt2(arrivalTime, depTime);
}

function gt2(d2, d1) {
    var diff = d2 - d1, sign = diff < 0 ? -1 : 1, milliseconds, seconds, minutes, hours, days;
    diff /= sign; // or diff=Math.abs(diff);
    diff = (diff - (milliseconds = diff % 1000)) / 1000;
    diff = (diff - (seconds = diff % 60)) / 60;
    diff = (diff - (minutes = diff % 60)) / 60;
    days = (diff - (hours = diff % 24)) / 24;
    return days + " Days " + hours + ":" + minutes + ":" + seconds + "0";
}

var a1 = new Flight('Delta', 'DAL42', 'Dallas, TX', 'San Antonio, TX', new Date(2018, 5, 15, 9, 15, 0), new Date(2018, 5, 15, 11, 40, 0), 'S5');
var a2 = new Flight('Tarom', 'TR52', 'Amsterdam, NL', 'Bucharest, RO', new Date(2018, 5, 15, 3, 36, 0), new Date(2018, 5, 15, 5, 32, 0), 'C2');
var a3 = new Flight('Air Canada', 'ACA87', 'Vancouver, B.C.', 'Amsterdam, NL', new Date(2018, 5, 15, 1, 10, 0), new Date(2018, 5, 15, 11, 40, 0), 'E75');
var a4 = new Flight('SkyWest', 'SW10', 'Minneapolis, MN', 'San Francisco, CA', new Date(2018, 5, 15, 3, 12, 0), new Date(2018, 5, 15, 5, 46, 0), 'A49');
var a5 = new Flight('WestJet', 'WJ21', 'Las Vegas, NV', 'Vancouver, B.C.', new Date(2018, 5, 15, 1, 38, 0), new Date(2018, 5, 15, 4, 26, 0), 'D10');

var arr = [a1, a2, a3, a4, a5,];

var tbody = $("#tablebody");

$.each(arr, function (key, value) {
    var tr = $("<tr />");
    $.each(value, function (k, v) {
        $("<td />", {
            html: "<p>" + v + "</p>"
        }).appendTo(tr)
    });
    tbody.append(tr)
})