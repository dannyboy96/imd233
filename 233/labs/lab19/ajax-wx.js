// Shorthand for $( document ).ready()
$(function() {
  // weather update button click
  $('button').on('click', function(e) {
    $('ul li').each(function() {
      console.log("this:" + this);
      $(this).remove();
    });
    $.ajax({
      processing: false,
      url: "http://api.wunderground.com/api/806d198d64f269c1/geolookup/conditions/q/WA/Bothell.json",
      dataType: "jsonp",
      beforeSend: function() { 
        //prevent multiple requests after first one was sent
        processing = true;
      },
      success: function(parsed_json) {
        if (processing) {

        var city = parsed_json['location']['city'];
        var state = parsed_json['location']['state'];
        var temp_f = parsed_json['current_observation']['temp_f'];
        var rh = parsed_json['current_observation']['relative_humidity'];
        var str = "<li> Location : " + city + ", " + state + "</li>";
        var weather = parsed_json['current_observation']['weather'];
        var icon_url = parsed_json['current_observation']['icon_url'];
        var wind = parsed_json['current_observation']['wind_degrees'] + "&#176;, " + parsed_json['current_observation']['wind_dir'] + " " + parsed_json['current_observation']['wind_mph'] + " mph";
        
        console.log("Current temperature in " + city + " is: " + temp_f);
        console.log(parsed_json);

        // Update list items with data from above...
        $('ul').append(str);
        $('ul li:last').attr('class', 'list-group-item');
        str = "<li> Current temperature: " + temp_f + "&#176; F" + "</li>";

        $('ul').append(str);
        $('ul li:last').attr('class', 'list-group-item');
        str = "<li> Relative Humidity: " + rh + "</li>";

        $('ul').append(str);
        $('ul li:last').attr('class', 'list-group-item');
        str = "<li> Current weather: " + weather + " <img src=" + icon_url + "></img></li>";

        $('ul').append(str);
        $('ul li:last').attr('class', 'list-group-item');
        str = "<li> Wind: " + wind + "</li>";

        $('ul').append(str);
        $('ul li:last').attr('class', 'list-group-item');
        processing = false;
        }

      }
    });
  });
});